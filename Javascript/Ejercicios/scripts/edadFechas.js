function CalcularEdad() 
{
	var dn,mn,an,
	    da,ma,aa,
	    resultado;

	dn = parseInt(document.getElementById('diaN').value);
	mn = parseInt(document.getElementById('mesN').value);
	an = parseInt(document.getElementById('anioN').value);
	da = parseInt(document.getElementById('diaA').value);
	ma = parseInt(document.getElementById('mesA').value);
	aa = parseInt(document.getElementById('anioA').value);
	resultado = document.getElementById('resultado');

	/* Comprobar que la fecha actual es mayor que la
	   fecha de nacimiento */
	if ( (ma > mn) || 
		 ((ma==mn) && (da >= dn)) )
		resultado.innerHTML += 
	       "La edad es  " + (aa-an) + " años"; 
	else
		resultado.innerHTML += 
	       "La edad es  " + ((aa-an)-1) + " años"; 
}