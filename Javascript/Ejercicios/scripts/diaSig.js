function CalcularDiaSig() 
{
	var dia,
	    mes,
	    anio,
	    resultado;

    dia  = parseInt(document.getElementById('diaA').value);
	mes  = parseInt(document.getElementById('mesA').value);
	anio = parseInt(document.getElementById('anioA').value);
	resultado = document.getElementById('resultado');

	if ( (dia==30) && ( (mes==4) || (mes==6) || 
		                (mes==9) || (mes==11) ) )
    {
    	dia = 1;
    	mes++;
    } 
    else if ((dia==31) && ((mes==1)||(mes==3)||(mes==5)|| 
    	                   (mes==7)||(mes==8)||(mes==10)))
    {
    	dia = 1;
    	mes++;
    }
    else if ((dia==31) && (mes==12))
    {
    	dia=1;
    	mes=1;
    	anio++;
    }
    else if ((dia==28) && (mes==2) && ((anio%4)!=0))
    {
    	dia=1;
    	mes++;
    }
    else if ((dia==29) && (mes==2) && ((anio%4)==0))
    {
    	dia=1;
    	mes++;
    }
    else
    {
    	dia++;
    }
    resultado.innerHTML =        
      "El dia siguiente es " + dia + "/" + mes + "/" + anio;
}
function CalcularDiaSigSW() 
{
	var dia,
	    mes,
	    anio,
	    resultado;

    dia  = parseInt(document.getElementById('diaA').value);
	mes  = parseInt(document.getElementById('mesA').value);
	anio = parseInt(document.getElementById('anioA').value);
	resultado = document.getElementById('resultado');

    switch(mes)
    {
    	case 4:
    	case 6:
    	case 9:
    	case 11: if (dia==30)
    	         {
    	         	dia=1;
    	         	mes++;
    	         }
    	         else
    	         	dia++;
    	         break; 
    	case 1:
    	case 3:
    	case 5:
    	case 7:
    	case 8:
    	case 10: if (dia==31)
    	         {
    	         	dia=1;
    	         	mes++;
    	         }
    	         else
    	         	dia++;
    	         break; 
    	case 12: if (dia==31)
    	         {
    	         	dia=1;
    	         	mes=1;
    	         	anio++;
    	         }
    	         else
    	         	dia++;
    	         break; 
    	case 2: if  ( ((dia==28) && (anio%4!=0)) || 
                      ((dia==29) && (anio%4==0)) ) 
    	        {
    	        	dia=1;
    	        	mes++;
    	        }
    	        else
    	        	dia++;
    	        break;
    }
    resultado.innerHTML =        
      "El dia siguiente es " + dia + "/" + mes + "/" + anio;
}