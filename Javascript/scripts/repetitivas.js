function CalcularTabla()
{
   var numero,
       contador,
       resultado;

  numero = 
     parseInt(document.getElementById('numero').value);
  resultado = document.getElementById('resultadoTabla');

  contador = 0;
  while (contador <= 10)  
  {
     resultado.innerHTML += 
       "<br>" + numero + " x " + contador + 
       " = " + (numero*contador); 
     contador++;
  }
}
function CalcularTablaDoWhile()
{
   var numero,
       contador,
       resultado;

  numero = 
     parseInt(document.getElementById('numeroDW').value);
  resultado = document.getElementById('resultadoTablaDW');

  contador = 0;
  resultado.innerHTML = "";
  do
  {
     resultado.innerHTML += 
       "<br>" + numero + " x " + contador + 
       " = " + (numero*contador); 
     contador++;
  }while (contador <= 10);
}

function CalcularTablaFor()
{
   var numero,
       contador,
       resultado;

  numero = 
     parseInt(document.getElementById('numeroF').value);
  resultado = document.getElementById('resultadoTablaF');

  for(contador = 0;contador <= 10;contador++)  
  {
     resultado.innerHTML += 
       "<br>" + numero + " x " + contador + 
       " = " + (numero*contador); 
  }
}
function EscribirDias() 
{
  var  tablaDias,
       contador,
       resultado;

  tablaDias = new Array();
  tablaDias[0] = "Lunes"; 
  tablaDias[1] = "Martes"; 
  tablaDias[2] = "Miércoles"; 
  tablaDias[3] = "Jueves"; 
  tablaDias[4] = "Viernes"; 
  tablaDias[5] = "Sábado"; 
  tablaDias[6] = "Domingo"; 

  resultado = document.getElementById('resultadoDias');

  // Recorrer tablaDias con for
  for(contador=0;contador<5;contador++)
  {
    resultado.innerHTML += "<br> " + tablaDias[contador];
  }
  // Recorrer tablaDias con for IN
  for(contador in tablaDias)
  {
    resultado.innerHTML += "<br> " + tablaDias[contador]; 
  }

  

}